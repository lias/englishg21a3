package com.starline.fertig;

import javax.swing.*;

import java.awt.*;

public class FertigForm extends JFrame{
	
	JPanel P1  = new JPanel();
	JLabel Text = new JLabel("Das Vokablquiz ist fertig!");
	JButton Beenden = new JButton("Beenden");
	ImageIcon ico = new ImageIcon("IconDatein/IconBild.PNG");
	
	public static void main(String args[])
	{
		new FertigForm();
	}

	public FertigForm()
	{
		super("English");
		setSize(180,100);
		setResizable(false);
		setLocationRelativeTo(null);
		
		P1.setBackground(Color.black);
		Text.setForeground(Color.lightGray);
		Beenden.setBackground(Color.gray);
		Beenden.setForeground(Color.red);

		Image icon = ico.getImage();
		setIconImage(icon);
		
		com.starline.basis.AbruchHandler abruch = new com.starline.basis.AbruchHandler();
		Beenden.addMouseListener(abruch);
		
		P1.add(Text);
		P1.add(Beenden);
		add(P1);
		
		setVisible(true);
	}
}
