package com.starline.frage3;

import java.awt.Color;
import java.awt.Image;

import javax.swing.*;

import java.awt.*;

public class FormFrage3 extends JFrame{

	JPanel P1 = new JPanel();
	JLabel Frage = new JLabel("Vokabel: musican");
	JTextField Antwort = new JTextField("Antwort", 23);
	JButton Abrechen = new JButton("Abrechen");
	JButton Kontrolle = new JButton("Weiter");
	ImageIcon ico = new ImageIcon("IconDatein/IconBild.PNG");
	
	public static void main(String args[])
	{
		new FormFrage3();
	}
	
	public FormFrage3()
	{
		super("English");
		setSize(280,110);
		setResizable(false);
		setLocationRelativeTo(null);
		
		P1.setBackground(Color.black);
		Frage.setForeground(Color.LIGHT_GRAY);
		Abrechen.setBackground(Color.gray);
		Kontrolle.setBackground(Color.gray);
		Kontrolle.setForeground(Color.red);
		
		P1.add(Frage);
		P1.add(Antwort);
		P1.add(Abrechen);
		P1.add(Kontrolle);
		add(P1);
		
		Image icon = ico.getImage();
		setIconImage(icon);
		
		com.starline.frage3.Frage3Weiter weiter = new com.starline.frage3.Frage3Weiter(this);
		Kontrolle.addMouseListener(weiter);
		com.starline.basis.AbruchHandler Abruch = new com.starline.basis.AbruchHandler();
		Abrechen.addMouseListener(Abruch);
		
		setVisible(true);
	}
}
