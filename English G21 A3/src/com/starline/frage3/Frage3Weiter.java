package com.starline.frage3;

import java.awt.event.*;

import javax.swing.JTextField;

public class Frage3Weiter implements MouseListener{

	FormFrage3 form;
	
	public Frage3Weiter(FormFrage3 form)
	{
		this.form = form;
	}
	
	public void mouseClicked(MouseEvent arg0) {
		weiter();
	}

	public void mouseEntered(MouseEvent arg0) {
	
	}

	public void mouseExited(MouseEvent arg0) {
	
	}

	public void mousePressed(MouseEvent arg0) {

	}

	public void mouseReleased(MouseEvent arg0) {
	
	}
	
	public void weiter()
	{
		JTextField antwort = (JTextField)form.P1.getComponent(1);
		if(antwort.getText().equals("Musiker"))
		{
			com.starline.fertig.FertigForm fertig = new com.starline.fertig.FertigForm();
			form.dispose();
		}
		else
		{
			com.starline.falsch.FalschForm falsch = new com.starline.falsch.FalschForm();
		}
	}
}
