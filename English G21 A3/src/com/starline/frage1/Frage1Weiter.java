package com.starline.frage1;

import java.awt.event.*;
import javax.swing.JTextField;

public class Frage1Weiter implements MouseListener{

	FormFrage1 form;
	
	public Frage1Weiter(FormFrage1 form)
	{
		this.form = form;
	}
	
	public void mouseClicked(MouseEvent arg0) {
	
		weiter();
	}

	public void mouseEntered(MouseEvent arg0) {

	}

	public void mouseExited(MouseEvent arg0) {

	}

	public void mousePressed(MouseEvent arg0) {

	}

	public void mouseReleased(MouseEvent arg0) {
	
	}
	
	public void weiter()
	{
		JTextField antwort = (JTextField)form.Panel1.getComponent(1);
		if(antwort.getText().equals("Einführung"))
		{
			com.starline.frage2.FormFrage2 form3 = new com.starline.frage2.FormFrage2();
			form.dispose();
		}
		else
		{
			com.starline.falsch.FalschForm falsch = new com.starline.falsch.FalschForm();
		}
	}
}
