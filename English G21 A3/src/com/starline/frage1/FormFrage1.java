package com.starline.frage1;

import javax.swing.*;
import java.awt.*;

public class FormFrage1 extends JFrame{

	public JPanel Panel1 = new JPanel();
	JLabel Frage = new JLabel("Vokabel: introduction");
	JTextField Antwort = new JTextField("Antwort", 23);
	JButton Kontrolle = new JButton("Weiter");
	JButton Abruch = new JButton("Abrechen");
	ImageIcon ico = new ImageIcon("IconDatein/IconBild.PNG");
	
	public static void main(String args[])
	{
		new FormFrage1();
	}
	
	public FormFrage1()
	{
		super("English");
		setSize(280,110);
		setResizable(false);
		setLocationRelativeTo(null);
		
		Panel1 = new JPanel();
		Panel1.setBackground(Color.black);
		Frage.setForeground(Color.LIGHT_GRAY);
		Abruch.setBackground(Color.gray);
		Abruch.setForeground(Color.LIGHT_GRAY);
		Kontrolle.setBackground(Color.gray);
		Kontrolle.setForeground(Color.red);
		
		Panel1.add(Frage);
		Panel1.add(Antwort);
		Panel1.add(Abruch);
		Panel1.add(Kontrolle);
		add(Panel1);
		
		Image icon = ico.getImage();
		setIconImage(icon);
		
		com.starline.basis.AbruchHandler handler1 = new com.starline.basis.AbruchHandler();
		Abruch.addMouseListener(handler1);
		Frage1Weiter handler2 = new Frage1Weiter(this);
		Kontrolle.addMouseListener(handler2);
		
		setVisible(true);
	}
}
