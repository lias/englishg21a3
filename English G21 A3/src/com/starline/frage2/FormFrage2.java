package com.starline.frage2;

import javax.swing.*;
import java.awt.*;

public class FormFrage2 extends JFrame{

	JPanel P1 = new JPanel();
	JLabel Frage = new JLabel("Vokabel: Jugend");
	JTextField Antwort = new JTextField("Antwort", 23);
	JButton Abrechen = new JButton("Abrechen");
	JButton Kontrolle = new JButton("Weiter");
	ImageIcon ico = new ImageIcon("IconDatein/IconBild.PNG");
	
	public static void main(String args[])
	{
		new FormFrage2();
	}
	
	public FormFrage2()
	{
		super("English");
		setSize(280,110);
		setResizable(false);
		setLocationRelativeTo(null);
		
		P1.setBackground(Color.black);
		Frage.setForeground(Color.LIGHT_GRAY);
		Abrechen.setBackground(Color.gray);
		Abrechen.setForeground(Color.LIGHT_GRAY);
		Kontrolle.setBackground(Color.gray);
		Kontrolle.setForeground(Color.red);
		
		P1.add(Frage);
		P1.add(Antwort);
		P1.add(Abrechen);
		P1.add(Kontrolle);
		add(P1);
		
		Image icon = ico.getImage();
		setIconImage(icon);
		
		com.starline.basis.AbruchHandler Abruch = new com.starline.basis.AbruchHandler();
		Abrechen.addMouseListener(Abruch);
		com.starline.frage2.Frage2Weiter formweiter = new com.starline.frage2.Frage2Weiter(this);
		Kontrolle.addMouseListener(formweiter);
		
		setVisible(true);
		
	}
}
