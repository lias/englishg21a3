package com.starline.basis;

import java.awt.event.*;

public class BasisWeiter implements MouseListener{

	FormBasis form;
	
	public BasisWeiter(FormBasis form)
	{
		this.form = form;
	}
	
	public void mouseClicked(MouseEvent e) {
		Weiter();
	}

	public void mouseEntered(MouseEvent e) {
	
	}

	public void mouseExited(MouseEvent e) {

	}

	public void mousePressed(MouseEvent e) {

	}

	public void mouseReleased(MouseEvent e) {

	}

	public void Weiter()
	{
		com.starline.frage1.FormFrage1 frage1neu = new com.starline.frage1.FormFrage1();
		form.dispose();
	}
}
