package com.starline.basis;

import javax.swing.*;
import javax.swing.ImageIcon;
import java.awt.*;

public class FormBasis extends JFrame{

	private JPanel Panel1 = new JPanel();
	JLabel Überschrift = new JLabel("Klicker");
	JLabel Text = new JLabel("Möchten Sie Klicker wirklich starten?");
	JButton Abruch = new JButton("Abrechen");
	JButton Weiter = new JButton("Starten");
	ImageIcon ico = new ImageIcon("IconDatein/IconBild.PNG");

	public static void main(String args[])
	{
		new FormBasis();
	}
	
	public FormBasis()
	{
		super("English");
		setSize(280,110);
		setResizable(false);
		setLocationRelativeTo(null);
		
		Panel1 = new JPanel();
		Panel1.setBackground(Color.black);
		Abruch.setBackground(Color.gray);
		Abruch.setForeground(Color.LIGHT_GRAY);
		Weiter.setBackground(Color.gray);
		Weiter.setForeground(Color.red);
		Überschrift.setForeground(Color.lightGray);
		Text.setForeground(Color.lightGray);
		
		Panel1.add(Überschrift);
		Panel1.add(Text);
		Panel1.add(Abruch);
		Panel1.add(Weiter);
		add(Panel1);
		
		Image icon = ico.getImage();
		setIconImage(icon);
		
		AbruchHandler handlerabrechen1 = new AbruchHandler();
		Abruch.addMouseListener(handlerabrechen1);
		
		BasisWeiter weiter = new BasisWeiter(this);
		Weiter.addMouseListener(weiter);
		
		setVisible(true);
	}
}
