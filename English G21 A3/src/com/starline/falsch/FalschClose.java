package com.starline.falsch;

import java.awt.event.*;

public class FalschClose implements MouseListener {

	FalschForm form;
	
	public FalschClose(FalschForm form)
	{
		this.form = form;
	}
	
	public void mouseClicked(MouseEvent arg0) {
		close();
	}

	public void mouseEntered(MouseEvent arg0) {

	}

	public void mouseExited(MouseEvent arg0) {

	}

	public void mousePressed(MouseEvent arg0) {

	}

	public void mouseReleased(MouseEvent arg0) {

	}
	
	public void close()
	{
		form.dispose();
	}
}
