package com.starline.falsch;

import javax.swing.*;

import java.awt.*;

public class FalschForm extends JFrame{

	JPanel P1 = new JPanel();
	JLabel Warnung = new JLabel("Die Übersetzung ist falsch! Lerne die Vokabel noch einmal!");
	JButton Abrechen = new JButton("Schließen");
	JButton Beenden = new JButton("Spiel beenden");
	ImageIcon ico = new ImageIcon("IconDatein/IconBild.PNG");
	
	public static void main(String ars[])
	{
		new FalschForm();
	}
	
	public FalschForm()
	{
		super("Falsch!");
		setSize(450,100);
		setResizable(false);
		setLocationRelativeTo(null);
		
		P1.setBackground(Color.black);
		Warnung.setForeground(Color.LIGHT_GRAY);
		Abrechen.setBackground(Color.gray);
		Abrechen.setForeground(Color.red);
		Beenden.setBackground(Color.GRAY);
		
		Image icon = ico.getImage();
		setIconImage(icon);
		
		com.starline.basis.AbruchHandler beenden = new com.starline.basis.AbruchHandler();
		Beenden.addMouseListener(beenden);
		com.starline.falsch.FalschClose close = new com.starline.falsch.FalschClose(this);
		Abrechen.addMouseListener(close);
		
		P1.add(Warnung);
		P1.add(Abrechen);
		P1.add(Beenden);
		add(P1);
		
		setVisible(true);
	}
}
